from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from IPython.display import display
from PIL import Image

print("# Initializing the classifier...")

classifier = Sequential()

classifier.add(Convolution2D(32, 3, 3, input_shape=(64,64,3), activation='relu'))
classifier.add(MaxPooling2D(pool_size=(2,2)))
classifier.add(Flatten())

classifier.add(Dense(output_dim=128, activation='relu'))
classifier.add(Dense(output_dim=1, activation='sigmoid'))

classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

print("# Training data...")
train_datagen = ImageDataGenerator(
	rescale=1./255, 
	shear_range=0.2,
	zoom_range=0.2,
	horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

print("# Testing data...?")

training_set = train_datagen.flow_from_directory(
	'dataset/training_set',
	target_size=(64, 64),
	batch_size=32,
	class_mode='binary')

test_set = test_datagen.flow_from_directory(
	'dataset/test_set',
	target_size=(64, 64),
	batch_size=32,
	class_mode='binary')

print("# Fitting the classifier...")

classifier.fit_generator(
	training_set,
	steps_per_epoch=1000,
	epochs=5,
	validation_data=test_set,
	validation_steps=300)

print("Finished")